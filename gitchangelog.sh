#!/bin/sh
# Copyright © 2013,2014 Géraud Meyer <graud@gmx.com>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License version 2 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

PROGRAM_NAME="gitchangelog.sh"
PROGRAM_VERSION="1.3"
help_usage () {
	cat <<"HelpMessage"
Name
	gitchangelog.sh - portable script generating a GNU-like changelog from a Git log
Usage
	gitchangelog.sh [<options>] [--] [ {-|<outfile>} [<git_log_args>] ]
	gitchangelog.sh [<options>] [--]   {-|<outfile>} -
Options
 	--tags
		Prepend changelog entries by tag names in brackets.
 	--tag-pattern <sed_BRE>
		Tags to consider for marking entries.  The pattern should not match
		across spaces; the default is "[^ ]\{1,\}".
 	--merge
		Prepend changelog entries by merge marks.
 	--title-only
		Only keep the title of a commit.  This implies --title-star, that can
		be disabled by a supplemental --git-body.
 	--title-star
		Prepend the title by a star as is done with the other parapgraphs.
 	--git-body
		Do not reformat the commit message, only reindent it.
 	--no-blankline
		Do not separate the title from the rest by a blank line.
 	--version
 	--help, --helpm
HelpMessage
}
help_message () {
	help_usage
	cat <<"HelpMessage"
Description
	Each commit message is made into a changelog entry.  Each paragraph of a
	Git commit message body is made into a star item of the changelog.  The
	message title is kept on a separate line.  Text lines are never broken nor
	joined.
	
	By default git is run to get the log, with the required options.  If a dash
	is given in place of additional <git_log_args>, then the log is read from
	the standard input; in that case pass the option --date=short and possibly
	also --decorate to git to get the appropriate log format.
Portability
	The script is portable to POSIX sh and to other less comformant shells such
	as pdksh.
	
	The sed code is portable to POSIX sed but requires only Simple Regular
	Expressions (SRE) instead of Basic Regular Expressions (BRE) which makes it
	prone to work under Solaris and old BSDs.  git-log is required if the log
	is not read from stdin.
Examples
	 $ ./gitchangelog.sh --title-star --no-blankline GNUChangeLog
	 $ ./gitchangelog.sh --tags --tag-pattern 'release\/[^ ]*' -- - --date-order |
	   sed 's/^\[release/^L\[release/' > ChangeLog
	 $ git log --date=short --decorate --first-parent |
	   ./gitchangelog.sh --merge ChangeLog -
Author
	gitchangelog.sh was written by G.raud Meyer.
See also
	git-log(1)
HelpMessage
}

# parameters
GITBODY= NO_GITBODY=yes
BLANKLINE=yes
TAGS=
TAG_PATTERN='[^ ]\+'
MERGE=
TITLE= NO_TITLE=yes
TITLESTAR=
while [ $# -gt 0 ]
do
	case $1 in
	--tags)
		TAGS=yes ;;
	--tag-pattern)
		shift; TAG_PATTERN=${1-} ;;
	--merge)
		MERGE=yes ;;
	--title-only)
		TITLE=yes NO_TITLE= TITLESTAR=yes ;;
	--title-star)
		TITLESTAR=yes ;;
	--git-body)
		GITBODY=yes NO_GITBODY= ;;
	--no-blankline)
		BLANKLINE= ;;
	--version)
		echo "$PROGRAM_NAME version $PROGRAM_VERSION"
		exit ;;
	--help)
		help_usage; exit ;;
	--helpm)
		help_message; exit ;;
	--?*)
		echo "$0: unknown option $1" >&2
		exit 255 ;;
	--)
		shift; break ;;
	*)
		break ;;
	esac
	shift
done
ANNOTATE=
[ -n "$TAGS" ] || [ -n "$MERGE" ] && ANNOTATE=yes
# git repository check
test x"${2-}" != x"-" &&
if test -d .git || git rev-parse --git-dir >/dev/null
then :
else
	echo "$0: error not in a git repository" >&2
	exit 255
fi
# output file
if test $# -gt 0
then
	test x"${1-}" != x"-" &&
	exec >"$1"
	shift
fi

# input source
B='{' b='}'
s='\'
  # Some shells, like the family of pdksh, behave differently regarding
  # the backslash in variable substitutions inside a here document.
NL='
'
  # pdksh and posh do not keep a newline preceded by a backslash inside a
  # single quoted string inside two levels of command substitution (when the
  # second level is double quoted).
exec 4>&1 # to pass stdout to the following process substitution
eval "$( exec 3>&1 1>&4 4>&- # eval will set $gitrc and $rc
if test x"${1-}" = x"-"
then cat; echo >&3 gitrc=$?
else git log --date=short ${ANNOTATE:+--decorate} ${1+"$@"}; echo >&3 gitrc=$?
fi |
# processing
LC_ALL=C sed -e "$(# Transform the GNU sed program into a more portable one
  LC_ALL=C sed -e 's/\\s/[ \\t]/g
    s/\\+/\\{1,\\}/g
    s/; /\'"$NL"'/g
    s/@n/\\\'"$NL"'/g
    s/\\t/	/g
    s/^ *#/#/' <<EOF
  # Put the tags in the hold space
  /^commit / {
    # parse the refs
    s/^commit [a-zA-Z0-9]\+//; s/^ \+(\(.*\))\$/, \1/
    s/^/@n/; s/, /@n /g
    # conditionnal branch to reset
    ${TAGS:+t a;} s/.*//; t e
    # extract the desired tags
    :a; s/\n\$//; t e
        s/\(.*\)\n tag: \($TAG_PATTERN\)\$/[\2]@n\1/; t a
        s/\(.*\)\n [^ ]*\$/\1/; t a
        s/\(.*\)\n [a-zA-Z]\+: *[^ ]*\$/\1/; t a
    :e; s/\n/ /g; h; d
  }
  # Add the merge marker to the hold space
  /^Merge: / { ${MERGE:+x; s/\$/${B}M$b /; x;} d; }
  /^Author:/ {
    # Process author, date, tag
    ${ANNOTATE:+x; s/${s}s*\$/@n/; x; b s;} x; s/.*/@n/; x${ANNOTATE:+; :s}
    N; G; s/^Author:\s*\(.*\) \s*\(<[^<>]*>\)\s*\nDate:\s*\(.*\)\n\(.*\n\)\$/\4\3  \1  \2@n/
    s/^\n//
    # Process title
    n; N; s/^.*\n    /\t${TITLESTAR:+${NO_GITBODY:+* }}/
    t t; :t; n; s/^    \(.\+\)\$/\t${TITLESTAR:+${NO_GITBODY:+  }}\1/; t t
    # If non empty body, print an extra line
    ${NO_TITLE:+${NO_GITBODY:+${BLANKLINE:+/^\$/ !$B s/^    /${s}t/p; s/^${s}t/    /; $b}}}
  }
  ${TITLE:+/^    / d}
  # First line of the paragraph
  :b; /^    \$/ { N; s/^    \n//; s/^    \(.\)/${GITBODY:+${s}t@n}\t${NO_GITBODY:+* }\1/; b b; }
  # Rest of the paragraph
  s/^    /\t${NO_GITBODY:+  }/
  # Reset the hold space for the next entry
  /^\$/ h
EOF
)" # sed
echo >&3 rc=$?
)" # eval
exec 4>&-

# error check
test $gitrc -eq 0 ||
echo >&2 "$0: ERROR git or cat failed with #$gitrc"
test $rc -eq 0 ||
{ echo >&2 "$0: ERROR sed failed with #$rc"; exit $rc; }
exit $gitrc
